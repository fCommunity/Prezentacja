<p align="center">
<img src="https://fcraft.pl/images/custom/hServer.png" alt="fCraft.pl - Minecraft 1.11.2"><br>
<b>Mniej administracji, więcej wolności!</b>
</p>

**Witaj w prezentacji serwera fCraft.pl** dodającego już na początku automatyczne logowanie z możliwością bezstratnej zmiany nicku dla graczy premium oraz skiny dla graczy non-premium.

**Poniższa prezentacja wprowadzi Cię w podstawowe aspekty serwera** - dalsze możliwości poznawania go oferuje kompletny system interaktywnej pomocy w grze dostępny pod komendą /pomoc, a także [rozbudowane forum](https://fcraft.pl/) tworzone w głównej mierze przez społeczność graczy.

W przypadku jakichkolwiek problemów z dołączeniem na serwer warto zajrzeć w poniższy spoiler:

[spoiler]

- **Access token cannot be null or empty.** - Użyj konta premium lub jeśli grasz na non-premium wybierz jakiś wolny (AVAILABLE!) nick na podstawie strony https://iaero.me/mcchecker.
- **Your country is banned from this server!** - Skontaktuj się z administracją (https://www.facebook.com/fcraftpl/ lub https://fcraft.pl/private.php?action=send&amp;uid=1), aby Twoje konto zostało ręcznie zarejestrowane lub kraj dodany na listę dozwolonych.
- **Inny błąd.** - Również zalecany jest kontakt z administracją (https://www.facebook.com/fcraftpl/ lub https://fcraft.pl/private.php?action=send&amp;uid=1).

[/spoiler]

<p align="center">
<img src="https://fcraft.pl/images/custom/hFreedom.png" alt="Wolność">
</p>
**Zapewniona jest pełna wolność w budowaniu i handlu oraz równe szanse w rozgrywce niezależnie od rangi.** Dodatkowo zgodnie z mottem serwera "Mniej administracji, więcej wolności!" poza właścicielem istnieje wyłącznie moderacja z minimalnymi uprawnieniami na potrzeby pomocy graczom - bez możliwości ich szpiegowania lub narzucania się w inny sposób. 

<p align="center">
<img src="https://fcraft.pl/images/custom/hProperty.png" alt="Własność">
</p>
**Każda Twoja budowla może zostać zabezpieczona cuboidem**, którym można bardzo łatwo zarządzać za pomocą komendy /c dostępnej w języku polskim. Dodatkowo dla bezpieczeństwa wszelkie zmiany bloków i skrzyń są zapisywane co pozwala na łatwe wycofanie szkód w przypadku kradzieży lub griefu.

**Mapa w całości tworzona przez graczy jest bardzo ważnym elementem serwera.** Obecnie nie są planowane jej resety, a nowe tereny gotowe do eksploracji pozyskiwane są przez powiększanie granicy świata.

**Kilka budowli z serwera:**

[spoiler]

Pałac:

<img src="https://i.imgur.com/jbUJV8H.png" alt="Pałac">

Przebudowana wioska:

<img src="https://i.imgur.com/BvjZYnR.jpg" alt="Przebudowana wioska">

Targowisko pod muchomorem:

<img src="https://i.imgur.com/UXt7dHl.png" alt="Targowisko pod muchomorem">

[/spoiler]

<p align="center">
<img src="https://fcraft.pl/images/custom/hEconomy.png" alt="Ekonomia">
</p>
**Serwer zapewnia bardzo realistyczny system ekonomiczny tworzony wyłącznie przez graczy** bez sztucznych ingerencji ze strony serwera. Serwerowa waluta ($) jest oparta na złocie wymienialnym komendą /bank. To gracze tworzą sklepy i oferty handlowe, a czasem nawet organizują konkursy jak np. parkoury z nagrodami lub pojedynki PvP. Dodatkowy atut stanowi stronicowana i pogrupowana historia wszelkich dokonanych transakcji, sprzedaży i zakupów dostępna pod komendą /historia.

<p align="center">
<img src="https://fcraft.pl/images/custom/hSurvival.png" alt="Survival">
</p>
**Nowi gracze pojawiają się w losowym miejscu na mapie** - ich pierwszym zadaniem jest przetrwanie, zdobycie jedzenia, osiedlenie się. Surowce na serwerze są oczywiście pozyskiwane z rud, a z kamienia wypada wyłącznie bruk (lub kamień w przypadku zaklęcia jedwabny dotyk).

<p align="center">
<img src="https://fcraft.pl/images/custom/hTransport.png" alt="Transport">
</p>
**Eliminacja teleportacji z rozgrywki przyczyniła się do rozwoju bardzo rozbudowanego systemu transportu** - gracze budują kilometry dróg, a czasem nawet torów kolejowych. Dzięki wykorzystaniu znanej mechaniki gry (1 kratka w netherze to 8 kratek w normalnym świecie) drogi i kolejki w netherze pozwalają szybko przedostać się nawet na drugi kraniec mapy.

**Dodatkową ciekawostką jest system wagi uzależniający szybkość poruszania się graczy od ilości bloków i przedmiotów w ekwipunku.** Stanowi to sporą zachętę do wykorzystywania mułów, osłów i lam czyli zwierząt zdolnych do transportu przedmiotów. Istnieje także możliwość podpisania siodła (Komenda: /podpisz) przed założeniem na konia lub innego wierzchowca, aby zachować możliwość wsiadania na cudzych cuboidach oraz ochronić go przed otrzymywaniem obrażeń.

**Screeny kilku dróg na serwerze:**

[spoiler]

Główna droga w zaludnionym miejscu:

<img src="https://i.imgur.com/AMgygXV.png" alt="Główna droga w zaludnionym miejscu">

Mosty na jednym z traktów:

<img src="https://i.imgur.com/fLqme6u.png" alt="Mosty na jednym z traktów">

Skrzyżowanie w mało zaludnionym miejscu:

<img src="https://i.imgur.com/GJrE4xS.png" alt="Skrzyżowanie w mało zaludnionym miejscu">

Portale połączone ścieżkami:

<img src="https://i.imgur.com/r96fQrl.jpg" alt="Portale połączone ścieżkami">

Kolejka w netherze:

<img src="https://i.imgur.com/pZcha3P.png" alt="Kolejka w netherze">

[/spoiler]

<p align="center">
<img src="https://fcraft.pl/images/custom/hAddons.png" alt="Dodatki">
</p>
**Na serwerze można znaleźć wiele dodatków znacznie polepszających jakość rozgrywki**, a ich lista dynamicznie poszerza się wraz z każdą aktualizacją! Oto niektóre z nich:

- Możliwość zamiany głowy moba na jedną z niemal 300 główek dekoracyjnych komendą /head.
- [Dodatkowa możliwość zdobywania Elytr i Shulkerów.](https://fcraft.pl/temat-przywo%C5%82ywanie-shulker%C3%B3w-264)
- [Kolorowe pisanie na tabliczkach.](https://fcraft.pl/temat-kolorowe-pisanie-na-tabliczkach-338)
- [Tworzenie bram z płotków, krat lub szyb.](https://fcraft.pl/temat-tworzenie-bram-259)
- Czcionka z obsługą polskich znaków w Minecraftowym stylu.
- Dobrowolne PvP włączane /pvp on lub wyłączane /pvp off.
- Odpowiednia szansa na drop głowy z niemal każdego moba.
- Zdobywanie głowy przeciwnika trzymając głowę moba w drugiej ręce podczas walki.
- Zamiana [pos] na chacie w aktualną pozycję gracza.
- Zdejmowanie właściciela z psów i kotów przez kliknięcie drewnianą siekierą.
- Wkładanie przedmiotów do rąk stojaków na zbroję.
- Siadanie na schodkach.
- I wiele, wiele innych... to dopiero początek!

<p align="center">
<img src="https://fcraft.pl/images/custom/hServer.png" alt="fCraft.pl - Minecraft 1.11.2"><br>
<b>Dołącz do naszej społeczności!</b><br>
<b>IP:</b> fCraft.pl
&nbsp;&nbsp;&nbsp;
<b>FB:</b> <a href="https://www.facebook.com/fcraftpl/">fb.com/fCraftPL</a>
&nbsp;&nbsp;&nbsp;
<b>Discord:</b> <a href="https://fcraft.pl/discord">fCraft.pl/discord</a>
</p>